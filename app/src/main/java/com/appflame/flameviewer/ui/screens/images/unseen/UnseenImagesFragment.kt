package com.appflame.flameviewer.ui.screens.images.unseen

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.appflame.flameviewer.ui.screens.images.BaseImagesFragment

class UnseenImagesFragment: BaseImagesFragment() {

    override var actionFun: (imageId: Int) -> Unit = { markImageAsViewed(it) }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        imagesViewModel.unseenImagesLiveData.observe(viewLifecycleOwner, Observer {
            imagesAdapter.addImages(it)
        })
    }

    private fun markImageAsViewed(imageId: Int) {
        imagesViewModel.markImageAsViewed(imageId)
    }

}