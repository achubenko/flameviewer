package com.appflame.flameviewer.ui.screens.images

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.appflame.flameviewer.MainActivity
import com.appflame.flameviewer.databinding.ImageHolderBinding
import com.appflame.flameviewer.databinding.ProgressbarBinding
import com.appflame.flameviewer.ui.model.PicsumImageUi
import com.bumptech.glide.Glide
import kotlinx.coroutines.*

class ImagesAdapter(
    val loadImagesForPage: (page: Int) -> Unit,
    val holderFinalClickAction: (picsumImageUi: PicsumImageUi) -> Unit,
    val fragmentScope: LifecycleCoroutineScope
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    lateinit var recyclerView: RecyclerView
    lateinit var layoutManager: GridLayoutManager
    private var isLoading: Boolean = false
    private var defSpanSize: Int = 1

    private val VIEW_ITEM = 1
    private val VIEW_PROG = 0

    private var imageDataList: MutableList<PicsumImageUi> = ArrayList()

    init {
        imageDataList.add(PicsumImageUi(-1,"stub", false, "stub", -1))
    }

    fun addImages(value: List<PicsumImageUi>) {
        val sizeBeforeAdding = imageDataList.size
        Log.d(ADAPTER_TAG, "addImages: value $value")
        isLoading = false
        imageDataList.addAll(imageDataList.size -1, value)
//        notifyDataSetChanged()
        notifyItemInserted(sizeBeforeAdding - 1)
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {

        super.onAttachedToRecyclerView(recyclerView)
        this.recyclerView = recyclerView
        layoutManager = recyclerView.layoutManager as GridLayoutManager
        defSpanSize = layoutManager.spanCount

//        layoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
//            override fun getSpanSize(position: Int): Int {
//                return if(position == imageDataList.size - 1){
//                    Log.d(ADAPTER_TAG, "getSpanSize if")
//                    1
//                } else {
//                    Log.d(ADAPTER_TAG, "getSpanSize else ${defSpanSize}")
//                    defSpanSize
//                }
//            }
//
//        }

        recyclerView.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                Log.d(
                    ADAPTER_TAG, "onScrolled, " +
                            "imageDataList.size: ${imageDataList.size}, " +
                            "lm.itemCount: ${layoutManager.itemCount}, " +
                            "lm.findLastVisibleItemPosition(): ${layoutManager.findLastVisibleItemPosition()}"
                )
                if (!isLoading && layoutManager.itemCount <= layoutManager.findLastVisibleItemPosition() + 1) {
                    Log.d(ADAPTER_TAG, "onScrolled, inside")
                    isLoading = true
                    imageDataList.asSequence().findLast { it.page != -1 }?.page?.let {
                        loadImagesForPage(
                            it + 1
                        )
                    }.run { loadImagesForPage }
                }
            }
        })
    }



    override fun getItemViewType(position: Int): Int {
        val visibleItemCount = layoutManager.childCount
        val totalItemCount = layoutManager.itemCount
        val pastVisibleItems = layoutManager.findFirstVisibleItemPosition()
        return if (imageDataList[position].id != -1) {
            Log.d(
                ADAPTER_TAG, "getItemViewType," +
                        " imageDataList.lastIndex: ${imageDataList.lastIndex}, " +
                        "position: ${position}"
            )
            VIEW_ITEM
        } else {
            Log.d(ADAPTER_TAG, "getItemViewType else")
//            loadImagesForPage(imageDataList.last().page + 1)
            VIEW_PROG
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (viewType == VIEW_ITEM) {
            ImageViewHolder(
                ImageHolderBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            )
        } else {
            ProgressBarViewHolder(
                ProgressbarBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        Log.d(ADAPTER_TAG, "onBindViewHolder, holder: $holder")
        if (holder is ImageViewHolder) {
            val imageData = imageDataList[position]
            holder.imageHolderBinding.authorTV.text = imageData.author
            holder.imageHolderBinding.idTv.text = imageData.id.toString()
            Glide.with(holder.imageHolderBinding.banner.context)
                .load(imageData.imageUrl)
                .centerCrop()
                .into(holder.imageHolderBinding.banner)
        } else {
            (holder as ProgressBarViewHolder).progressBarBinding.progressBar1.isIndeterminate = true
        }
    }

    override fun getItemCount(): Int {
        return imageDataList.size
    }

    companion object {
        val ADAPTER_TAG = ImagesAdapter::class.java.simpleName

    }

    inner class ImageViewHolder(
        val imageHolderBinding: ImageHolderBinding
        ) : RecyclerView.ViewHolder(imageHolderBinding.root) {

        init {
            tuneHolder()
        }

        private fun tuneHolder() {
            imageHolderBinding.root.setOnClickListener(object : View.OnClickListener {
                private var currentJob : Job? = null
                override fun onClick(v: View?) {
                    currentJob?.cancel()
                    currentJob = invokeTimerJob()
                }
            })
        }

        fun invokeTimerJob(totalSeconds: Int = 3 ): Job {
            return fragmentScope.launch() {
                for (second in totalSeconds downTo 0) {
                    if (second == 0) {
                        (imageHolderBinding.root.context as MainActivity).runOnUiThread {
                            holderFinalClickAction(
                                imageDataList.removeAt(adapterPosition).apply { isViewed = true }
                            )
                            notifyItemRemoved(adapterPosition)
                        }
                    } else {
                        (imageHolderBinding.root.context as MainActivity).runOnUiThread {
                            imageHolderBinding.timerTV.text = second.toString()
                        }

                    }
                    delay(1000)
                }
            }
        }

    }

    inner class ProgressBarViewHolder(val progressBarBinding: ProgressbarBinding) :
        RecyclerView.ViewHolder(progressBarBinding.root) {

    }

}