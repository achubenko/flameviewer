package com.appflame.flameviewer.ui.model

data class PicsumImageUi(
    val id: Int,
    val imageUrl: String,
    var isViewed: Boolean,
    val author: String,
    val page: Int
)