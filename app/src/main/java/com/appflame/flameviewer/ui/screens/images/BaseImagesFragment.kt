package com.appflame.flameviewer.ui.screens.images

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import com.appflame.flameviewer.R
import com.appflame.flameviewer.databinding.FragmentImagesBinding
import com.appflame.flameviewer.di.ViewModelFactory
import dagger.android.support.DaggerFragment
import javax.inject.Inject

abstract class BaseImagesFragment() : DaggerFragment() {

    protected lateinit var imagesViewModel: ImagesViewModel
    protected lateinit var imagesAdapter: ImagesAdapter
    protected lateinit var fragmentHomeBinding: FragmentImagesBinding
    protected abstract val actionFun: (imageId: Int) -> Unit

    @Inject lateinit var viewModelFactory: ViewModelFactory

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        imagesViewModel = ViewModelProvider(this, viewModelFactory).get(ImagesViewModel::class.java)
        fragmentHomeBinding = FragmentImagesBinding.inflate(inflater, container, false)
        return fragmentHomeBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tuneRecyclerView()
    }

    private fun tuneRecyclerView() {

        imagesAdapter = ImagesAdapter(
            { requestImagesForPage(it) },
            { actionFun(it.id) },
            lifecycleScope
        )

        fragmentHomeBinding.root.viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
            override fun onGlobalLayout() {
                val wishedGridSize =
                    requireContext().resources.getDimension(R.dimen.wished_image_holder_width)
                val windowWidth = fragmentHomeBinding.root.width
                val gridSize: Int = ( windowWidth / wishedGridSize + 1).toInt()
                Log.d(TAG, "tuneRecyclerView wishedGridSize: $wishedGridSize; windowWidth: $windowWidth; gridSize: $gridSize")


                fragmentHomeBinding.imagesRecycler.layoutManager = GridLayoutManager(context, gridSize)

                fragmentHomeBinding.imagesRecycler.adapter = imagesAdapter
                imagesViewModel.retrieveImagesForPage(0)
                fragmentHomeBinding.root.viewTreeObserver.removeOnGlobalLayoutListener(this)
            }
        })
    }

    private fun requestImagesForPage(page: Int) {
        Log.d(TAG, "requestImagesForPage $page")
        imagesViewModel.retrieveImagesForPage(page)
    }

    companion object {
        private val TAG = BaseImagesFragment::class.java.simpleName
    }

}