package com.appflame.flameviewer.ui.screens.images.viewed

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.appflame.flameviewer.ui.model.PicsumImageUi
import com.appflame.flameviewer.ui.screens.images.BaseImagesFragment

class ViewedImagesFragment : BaseImagesFragment() {

    override var actionFun: (imageId: Int) -> Unit = { markImageAsUnseen(it) }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        imagesViewModel.viewedImagesLiveData.observe(viewLifecycleOwner, Observer {
            imagesAdapter.addImages(it)
        })
    }

    private fun markImageAsUnseen(image: Int) {
        imagesViewModel.markImageAsUnseen(image)
    }

}