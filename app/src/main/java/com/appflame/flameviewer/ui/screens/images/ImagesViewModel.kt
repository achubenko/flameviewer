package com.appflame.flameviewer.ui.screens.images

import androidx.lifecycle.*
import com.appflame.flameviewer.domain.ViewerUseCase
import com.appflame.flameviewer.ui.model.PicsumImageUi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flatMapLatest
import kotlinx.coroutines.flow.switchMap
import kotlinx.coroutines.launch
import javax.inject.Inject

class ImagesViewModel @Inject constructor(val viewerUseCase: ViewerUseCase) : ViewModel() {

    private val pageFlow = MutableStateFlow(0)

    private val _imagesLiveData: LiveData<List<PicsumImageUi>> =
        pageFlow.flatMapLatest {
                pageFlow -> viewerUseCase.getPicsumImagesListForPage(pageFlow)
        }.asLiveData()

    val unseenImagesLiveData : LiveData<List<PicsumImageUi>> =
        _imagesLiveData.map { list -> list.filter { !it.isViewed } }

    val viewedImagesLiveData : LiveData<List<PicsumImageUi>> =
        _imagesLiveData.map { list -> list.filter { it.isViewed } }

    fun retrieveImagesForPage(page: Int = 0) {
        pageFlow.value = page
    }

    fun markImageAsViewed(imageId: Int) {
        viewModelScope.launch {
            viewerUseCase.saveImageIdAsViewed(imageId)
        }
    }

    fun markImageAsUnseen(imageId: Int) {
        viewModelScope.launch {
            viewerUseCase.deleteImageIdFromViewed(imageId)
        }
    }

}
