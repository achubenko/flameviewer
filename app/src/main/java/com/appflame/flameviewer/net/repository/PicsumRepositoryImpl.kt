package com.appflame.flameviewer.net.repository

import android.util.Log
import com.appflame.flameviewer.domain.ImageApiRepository
import com.appflame.flameviewer.net.model.PicsumImageNet
import com.appflame.flameviewer.net.client.PicsumClient
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.flow.map

class PicsumRepositoryImpl(private val picsumClient: PicsumClient): ImageApiRepository {

    override suspend fun getImageList(page: Int, limit: Int): Flow<List<PicsumImageNet>> {
        return flowOf(picsumClient.getImageList(page, limit)).map {
            Log.d(TAG, "it.isSuccessful: ${it.isSuccessful}")
            it.body()!! }
    }

    companion object {
        val TAG: String = PicsumRepositoryImpl::class.java.simpleName
    }

}