package com.appflame.flameviewer.net.client

import com.appflame.flameviewer.net.model.PicsumImageNet
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface PicsumClient {

    @GET("list")
    suspend fun getImageList(
        @Query("page") page_no: Int,
        @Query("limit") limit: Int
    ): Response<List<PicsumImageNet>>

    companion object {
        const val PICSUM_URL = "https://picsum.photos/v2/"
    }

}

