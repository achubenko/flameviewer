package com.appflame.flameviewer.net.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class PicsumImageNet(
    @Expose
    val id: Int,
    @Expose
    val author: String,
    @Expose
    val width: Int,
    @Expose
    val height: Int,
    @Expose
    val url: String,
    @Expose
    @SerializedName("download_url")
    val downloadUrl: String
)