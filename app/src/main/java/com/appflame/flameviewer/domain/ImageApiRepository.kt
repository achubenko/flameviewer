package com.appflame.flameviewer.domain

import com.appflame.flameviewer.net.model.PicsumImageNet
import kotlinx.coroutines.flow.Flow

interface ImageApiRepository {

    suspend fun getImageList(page: Int, limit: Int): Flow<List<PicsumImageNet>>

}