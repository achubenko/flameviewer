package com.appflame.flameviewer.domain

import android.util.Log
import com.appflame.flameviewer.data.model.ImageDataDb
import com.appflame.flameviewer.net.model.PicsumImageNet
import com.appflame.flameviewer.ui.model.PicsumImageUi
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.runBlocking
import javax.inject.Inject

class ViewerUseCase @Inject constructor(
    private val imageApiRepository: ImageApiRepository,
    private val imageDataRepository: ImageDataRepository
    ) {

    suspend fun getPicsumImagesListForPage(page: Int): Flow<List<PicsumImageUi>> {
        return imageApiRepository.getImageList(page, limit = 10).map { it -> markViewedImages(it, page) }
    }

    private fun markViewedImages(netEntities: List<PicsumImageNet>, page: Int): List<PicsumImageUi> {
        val viewedIds: List<Int> = runBlocking {
            imageDataRepository.getAllExistedImageData(netEntities.map { it.id })
                    .first().map { it.imageId }
        }

        return netEntities.map {it ->
            val picsumImageUi = PicsumImageUi(
                    id = it.id,
                    imageUrl = it.downloadUrl,
                    isViewed = viewedIds.contains(it.id),
                    author = it.author,
                    page = page
            )
            Log.i(TAG, "picsumImageUi: $picsumImageUi")
            picsumImageUi
        }
    }

    suspend fun saveImageIdAsViewed(id: Int) {
        imageDataRepository.addImageData(ImageDataDb(id))
    }

    suspend fun deleteImageIdFromViewed(id: Int) {
        imageDataRepository.deleteImageData(id)
    }

    companion object {
        val TAG = ViewerUseCase::class.java.simpleName
    }

}