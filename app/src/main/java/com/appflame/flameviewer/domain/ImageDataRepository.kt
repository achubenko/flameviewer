package com.appflame.flameviewer.domain

import com.appflame.flameviewer.data.model.ImageDataDb
import kotlinx.coroutines.flow.Flow

interface ImageDataRepository {

    fun getAllExistedImageData(ids: List<Int>): Flow<List<ImageDataDb>>

    suspend fun addImageData(imageData: ImageDataDb)

    suspend fun deleteImageData(id: Int)

}