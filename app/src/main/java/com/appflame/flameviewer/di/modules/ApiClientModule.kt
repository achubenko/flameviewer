package com.appflame.flameviewer.di.modules

import com.appflame.flameviewer.net.client.PicsumClient
import com.appflame.flameviewer.net.client.RetrofitProvider
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApiClientModule {

    @Singleton
    @Provides
    fun providesPicsumClient(retrofitProvider: RetrofitProvider): PicsumClient {
        return  retrofitProvider
            .getRetrofit(PicsumClient.PICSUM_URL)
            .create(PicsumClient::class.java)
    }

    @Singleton
    @Provides
    fun provideRetrofitProvider(): RetrofitProvider {
        return RetrofitProvider
    }
}