package com.appflame.flameviewer.di.modules

import com.appflame.flameviewer.di.scopes.FragmentScoped
import com.appflame.flameviewer.ui.screens.images.unseen.UnseenImagesFragment
import com.appflame.flameviewer.ui.screens.images.viewed.ViewedImagesFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun createUnseenImagesFragment(): UnseenImagesFragment

    @FragmentScoped
    @ContributesAndroidInjector
    abstract fun createViewedImagesFragment(): ViewedImagesFragment

}