package com.appflame.flameviewer.di.modules

import android.app.Application
import com.appflame.flameviewer.data.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class DbModule {

    @Provides
    @Singleton
    fun provideDatabase(application: Application): AppDatabase {
        return AppDatabase.build(application)
    }
}