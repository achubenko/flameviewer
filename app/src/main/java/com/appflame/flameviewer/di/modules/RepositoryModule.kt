package com.appflame.flameviewer.di.modules

import com.appflame.flameviewer.data.AppDatabase
import com.appflame.flameviewer.data.repository.ImageDataRepositoryImpl
import com.appflame.flameviewer.domain.ImageApiRepository
import com.appflame.flameviewer.domain.ImageDataRepository
import com.appflame.flameviewer.net.client.PicsumClient
import com.appflame.flameviewer.net.repository.PicsumRepositoryImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun providesImageDataRepository(database: AppDatabase): ImageDataRepository {
        return ImageDataRepositoryImpl(database.imageDataDao())
    }

    @Provides
    @Singleton
    fun providesImageApiRepository(picsumClient: PicsumClient): ImageApiRepository {
        return PicsumRepositoryImpl(picsumClient)
    }


}