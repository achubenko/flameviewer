package com.appflame.flameviewer.di.modules

import com.appflame.flameviewer.domain.ViewerUseCase
import dagger.Binds
import dagger.Module

//@Module
abstract class UseCaseModule {

//    @Binds
    abstract fun providesViewerUseCase(viewerUseCase: ViewerUseCase): ViewerUseCase
}