package com.appflame.flameviewer.di.components

import android.app.Application
import com.appflame.flameviewer.FlameViewerApp
import com.appflame.flameviewer.di.modules.*
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton


@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        DbModule::class,
        RepositoryModule::class,
//        UseCaseModule::class,
        ActivityModule::class,
        ViewModelModule::class,
        FragmentModule::class,
        ApiClientModule::class
    ]
)
interface AppComponent : AndroidInjector<FlameViewerApp> {
    @Component.Builder
    interface Builder {

        fun build(): AppComponent

        @BindsInstance
        fun application(application: Application): Builder

    }

}