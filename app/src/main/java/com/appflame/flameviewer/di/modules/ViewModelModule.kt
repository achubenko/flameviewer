package com.appflame.flameviewer.di.modules

import androidx.lifecycle.ViewModel
import com.appflame.flameviewer.di.ViewModelKey
import com.appflame.flameviewer.ui.screens.images.ImagesViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    @Binds
    @IntoMap
    @ViewModelKey(ImagesViewModel::class)
    abstract fun providesUnseenImagesViewModel(imagesViewModel: ImagesViewModel): ViewModel

}