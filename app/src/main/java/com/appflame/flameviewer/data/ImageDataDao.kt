package com.appflame.flameviewer.data

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.appflame.flameviewer.data.model.ImageDataDb
import kotlinx.coroutines.flow.Flow

@Dao
interface ImageDataDao {

    @Query("SELECT * FROM imagedata WHERE imageId in (:ids)")
    fun getAllExistedImageData(ids: List<Int>): Flow<List<ImageDataDb>>

    @Insert
    suspend fun addImageData(imageData: ImageDataDb)

    @Query("DELETE FROM imagedata WHERE imageId == :id")
    suspend fun deleteImageData(id: Int)
}