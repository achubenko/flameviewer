package com.appflame.flameviewer.data.repository

import com.appflame.flameviewer.data.ImageDataDao
import com.appflame.flameviewer.data.model.ImageDataDb
import com.appflame.flameviewer.domain.ImageDataRepository
import kotlinx.coroutines.flow.Flow

class ImageDataRepositoryImpl(val imageDataDao: ImageDataDao): ImageDataRepository {

    override fun getAllExistedImageData(ids: List<Int>): Flow<List<ImageDataDb>> = imageDataDao.getAllExistedImageData(ids)

    override suspend fun addImageData(imageData: ImageDataDb) = imageDataDao.addImageData(imageData)

    override suspend fun deleteImageData(id: Int) = imageDataDao.deleteImageData(id)
}