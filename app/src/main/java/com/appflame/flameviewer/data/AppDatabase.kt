package com.appflame.flameviewer.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.appflame.flameviewer.data.model.ImageDataDb

@Database(entities = [ImageDataDb::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun imageDataDao(): ImageDataDao

    companion object {
        val NAME = "app_database.db"

        fun build(context: Context): AppDatabase {
            return Room.databaseBuilder<AppDatabase>(context, AppDatabase::class.java, AppDatabase.NAME)
                    .addCallback(object : RoomDatabase.Callback() {
                        override fun onCreate(db: SupportSQLiteDatabase) {

                        }
                        override fun onOpen(db: SupportSQLiteDatabase) {

                        }
                    }).fallbackToDestructiveMigration()
                    .build()
        }
    }
}