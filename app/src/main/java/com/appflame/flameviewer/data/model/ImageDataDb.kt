package com.appflame.flameviewer.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "imagedata")
data class ImageDataDb(
    @PrimaryKey val imageId: Int
)